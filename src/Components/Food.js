import React from 'react';
import { RectButton } from 'react-native-gesture-handler'
import styled from 'styled-components/native';

const Food = () => {
  console.log('Food');
  return (
    <ItemBox>
      <FoodImage
        source={{
          uri: 'https://m.bonde.com.br/img/bondenews/2019/07/img_1358.jpg',
        }}
      />

      <NameRestaurant>Restaurante da Neide</NameRestaurant>
      <NameFood>Cuca de Leite Condensado</NameFood>

      <InfoBuy>
        <Price>R$ 12,00</Price>

        <ShopButton>
          <ShopText>COMPRAR</ShopText>
        </ShopButton>
      </InfoBuy>
    </ItemBox>
  );
};

export default Food;

const ItemBox = styled.View`
  background: #333;
  margin: 10px;
  padding: 16px;
  border-radius: 15px;
`;

const FoodImage = styled.Image`
  width: 100%;
  height: 300px;
  opacity: 0.8;
`;

const NameRestaurant = styled.Text`
  color: white;
  font-size: 12px;
`;

const NameFood = styled.Text`
  font-size: 18px;
  color: white;
`;

const InfoBuy = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Price = styled.Text`
  color: green;
  font-size: 15px;
  font-weight: bold;
`;

const ShopButton = styled(RectButton)`
  padding: 5px 10px;
  border-radius: 8px;
  background: green;
`;

const ShopText = styled.Text`
  font-size: 13px;
`;
