import React from 'react';
import styled from 'styled-components/native';

import Food from '../Components/Food';

const Feed = () => {
  console.log('Feed');
  return (
    <Container>
      <Food />
    </Container>
  );
};

export default Feed;

const Container = styled.ScrollView`
  background: black;
`;
