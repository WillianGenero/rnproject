import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Feed from './Pages/Feed';

const AppStack = createStackNavigator();

const Routes = () => {
  return (
    <NavigationContainer>
      <AppStack.Navigator headerMode="none" initialRouteName="Feed">
        <AppStack.Screen name="Feed" component={Feed} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
